'use strict'

import inquirer from 'inquirer';
import sqlite from 'sqlite3';
import tool from './library/tool.js';

// sqlite db物件
let db = null;

/**
 * 主程序
 */
function main() {
  db = new sqlite.Database('./application.db');
  // 資料表初始化
  db.serialize(() => {
    db.prepare(`CREATE TABLE IF NOT EXISTS order_table (order_id TEXT PRIMARY KEY, state TEXT NOT NULL DEFAULT undone, create_time TEXT NOT NULL)`).run().finalize();
    db.prepare(`CREATE TABLE IF NOT EXISTS order_detail_table (order_id TEXT, item TEXT, PRIMARY KEY (order_id, item))`).run().finalize();

    // 異步run方法
    db.runAsync = function(sql, params = []) {
      return new Promise((resolve, reject) => {
        this.run(sql, params, (error, result) => {
          if (error) {
            reject(error);
          }
          resolve(result);
        });
      });
    };

    // 異步all方法
    db.allAsync = function(sql, params = []) {
      return new Promise((resolve, reject) => {
        this.all(sql, params, (error, result) => {
          if (error) {
            reject(error);
          }
          resolve(result);
        });
      });
    };
  });

  // 主選單
  menu(db);

}

/**
 * 主選單方法
 * @param {object} db sqlite 物件
 * 
 */
function menu(db) {

  // 主選單問題物件
  const mainMenuQuestion = {
    type: 'list',
    message: '請選擇操作項目：',
    name: 'mainMenu',
    choices: ['查詢訂單', '新增訂單', '更新訂單', '關閉']
  };
  
  // 查詢訂單問題物件
  const selectOrderQuestion = {
    type: 'list',
    message: '請選擇查詢訂單項目：',
    name: 'selectOrder',
    choices: ['全部訂單', '未完成訂單', '完成訂單', '特定訂單', '取消'],
    when: function(answers) {
      return answers.mainMenu == '查詢訂單'
    }
  };

  // 查詢訂單 - 特定訂單問題物件
  const selectOrderByOrderIdQuestion = {
    type: 'name',
    message: '請輸入訂單編號：',
    name: 'selectOrderByOrderId',
    when: function(answers) {
      return answers.selectOrder == '特定訂單'
    },
    validate: function(value) {
      let bool = value.match(/^[0-9]/);
      if (!bool) {
        return '請輸入正確格式訂單編號'
      }
      return true
    }
  };

  // 新增訂單問題物件
  const insertOrderQuestion = {
    type: 'checkbox',
    message: '請選擇產品項目：',
    name: 'insertOrder',
    choices: [
      {
        name: '蛋餅',
        value: '蛋餅'
      },
      {
        name: '薯條',
        value: '薯條'
      },
      {
        name: '薯餅',
        value: '薯餅'
      },
      {
        name: '熱狗',
        value: '熱狗'
      },
      {
        name: '漢堡',
        value: '漢堡'
      }
    ],
    when: function(answers) {
      return answers.mainMenu == '新增訂單'
    },
    validate: function(value) {
      if (value.length == 0) {
        return '請選擇產品項目'
      }
      return true
    }
  };

  // 更新訂單問題物件
  const updateOrderByOrderIdQuestion = {
    type: 'name',
    message: '請輸入訂單編號：',
    name: 'updateOrderByOrderId',
    when: function(answers) {
      return answers.mainMenu == '更新訂單'
    },
    validate: function(value) {
      let bool = value.match(/^[0-9]/);
      if (!bool) {
        return '請輸入正確格式訂單編號'
      }
      return true
    }
  };

  // 問題陣列參數
  const questions = [
    mainMenuQuestion,
    selectOrderQuestion,
    selectOrderByOrderIdQuestion,
    insertOrderQuestion,
    updateOrderByOrderIdQuestion
  ];
  inquirer.prompt(questions).then(async (answers) => {

    switch(answers.mainMenu) {
      case '查詢訂單':
        await tool.selectOrder(answers, db);
        break;
      case '新增訂單':
        await tool.insertOrder(answers, db);
        break;
      case '更新訂單':
        await tool.updateOrder(answers, db);
        break;
    }

    // 若選擇關閉，程式即起結束
    if (answers.mainMenu == '關閉') {
      db.close();
      process.exit(0);
    }

    // 回到主選單
    menu(db);

  }).catch((error) => {
    console.log(`主選單發生錯誤: `, error);
  });
}

// 執行主程序
main();