'use strict'

import moment from 'moment';
import model from './model.js';

export default {
  /**
   * 處理查詢訂單方法
   * @param {object} answers inquirer answers 物件
   * @param {object} db sqlite 物件
   * 
   */
  async selectOrder(answers, db) {
    let result = []
    switch(answers.selectOrder) {
      case '全部訂單':
        try {
          result = await model.selectOrderByAll(db);
        } catch(error) {
          console.log(`查詢全部訂單發生錯誤: `, error);
          return;
        }
        break;
      case '未完成訂單':
        try {
          result = await model.selectOrderByState(db, 'undone');
        } catch(error) {
          console.log(`查詢未完成訂單發生錯誤: `, error);
          return;
        }
        break;
      case '完成訂單':
        try {
          result = await model.selectOrderByState(db, 'done');
        } catch(error) {
          console.log(`查詢完成訂單發生錯誤: `, error);
          return;
        }
        break;
      case '特定訂單':
        try {
          result = await model.selectOrderById(db, answers.selectOrderByOrderId);
        } catch(error) {
          console.log(`查詢特定訂單發生錯誤: `, error);
          return;
        }
        break;
    }
    if (result.length == 0) {
      console.log(`查無訂單`);
      return;
    }
    for (let i = 0; i < result.length; i++) {
      let state = '';
      switch(result[i].state) {
        case 'done':
          state = '完成';
          break;
        case 'undone':
          state = '未完成';
          break;
      }
      console.log(`訂單狀態：${state}, 訂單時間：${result[i].create_time}, 訂單編號：${result[i].order_id}, 產品項目：${result[i].item}`);
    }
  },
  /**
   * 處理新增訂單方法
   * @param {object} answers inquirer answers 物件
   * @param {object} db sqlite 物件
   * 
   */
  async insertOrder(answers, db) {
    let orderId = moment().format('YYYYMMDDHHmmss');
    let values = {
      orderId: orderId,
      createTime: moment().format('YYYY-MM-DD HH:mm:ss'),
      items: answers.insertOrder
    };
    let insertResult = await model.insertOrder(db, values);

    if (!insertResult) {
      console.log(`新增訂單發生錯誤`);
      return;
    }
    console.log(`新增訂單編號：`, orderId);
  },
  /**
   * 處理更新訂單方法
   * @param {object} answers inquirer answers 物件
   * @param {object} db sqlite 物件
   * 
   */
  async updateOrder(answers, db) {

    let result = await model.selectOrderById(db, answers.updateOrderByOrderId);

    if (result.length == 0) {
      console.log(`無該更新訂單編號`);
      return;
    }

    if (result[0].state == 'done') {
      console.log(`該更新訂單編號，訂單狀態為完成`);
      return;
    }

    try {
      await model.updateOrderByOrderId(db, answers.updateOrderByOrderId);
    } catch(error) {
      console.log(`更新訂單發生錯誤: `, error);
    }
    console.log(`更新訂單編號：`, answers.updateOrderByOrderId);
  }



};