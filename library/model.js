'use strict'

export default {
  /**
   * 查詢所有訂單
   * @param {object} db sqlite 物件
   * 
   * @returns {array} 查詢結果
   */
  selectOrderByAll(db) {
    let sql = `SELECT
                 ot.state,
                 ot.create_time,
                 ot.order_id,
                 GROUP_CONCAT(odt.item) AS item
               FROM order_table AS ot 
               JOIN order_detail_table AS odt ON ot.order_id = odt.order_id
               GROUP BY ot.order_id
               ORDER BY ot.state DESC, ot.create_time DESC, ot.order_id DESC`;
    let result = db.allAsync(sql);
    return result;
  },
  /**
   * 查詢所有訂單By狀態查詢
   * @param {object} db sqlite 物件
   * @param {string} state 訂單狀態；done：完成、undone：未完成
   * 
   * @returns {array} 查詢結果
   */
  selectOrderByState(db, state = 'done') {
    let sql = `SELECT
                 ot.state,
                 ot.create_time,
                 ot.order_id,
                 GROUP_CONCAT(odt.item) AS item
               FROM order_table AS ot 
               JOIN order_detail_table AS odt ON ot.order_id = odt.order_id
               WHERE ot.state = ?
               GROUP BY ot.order_id
               ORDER BY ot.state DESC, ot.create_time DESC, ot.order_id DESC`;
    let result = db.allAsync(sql, [state]);
    return result;
  },
  /**
   * 查詢特定訂單
   * @param {object} db sqlite 物件
   * @param {string} orderId 訂單編號
   * 
   * @returns {array} 查詢結果
   */
  selectOrderById(db, orderId) {
    let sql = `SELECT
                 ot.state,
                 ot.create_time,
                 ot.order_id,
                 GROUP_CONCAT(odt.item) AS item
               FROM order_table AS ot 
               JOIN order_detail_table AS odt ON ot.order_id = odt.order_id
               WHERE ot.order_id = ?
               GROUP BY ot.order_id
               ORDER BY ot.state DESC, ot.create_time DESC, ot.order_id DESC`;
    let result = db.allAsync(sql, [orderId]);
    return result;
  },
  /**
   * 新增訂單(交易模式)
   * @param {object} db sqlite 物件
   * @param {object} values 新增物件
   * 
   * @returns {array} 查詢結果
   */
  async insertOrder(db, values) {

    let sqlArray = [
      {
        sql: `BEGIN`,
        params: []
      },
      {
        sql: `INSERT INTO order_table (order_id, create_time) VALUES (?, ?)`,
        params: [values.orderId, values.createTime]
      }
    ];
    for (let i = 0; i < values.items.length; i++) {
      sqlArray.push(
        {
          sql: `INSERT INTO order_detail_table (order_id, item) VALUES (?, ?)`,
          params: [values.orderId, values.items[i]]
        }
      );
    }
    sqlArray.push(
      {
        sql: `COMMIT`,
        params: []
      }
    );

    for (let i = 0; i < sqlArray.length; i++) {
      try {
        await db.runAsync(sqlArray[i].sql, sqlArray[i].params);
      } catch(error) {
        await db.runAsync('ROLLBACK');
        return false;
      }
    }
    return true;
  },
  /**
   * 更新訂單
   * @param {object} db sqlite 物件
   * @param {string} orderId 訂單編號
   * 
   * @returns {array} 查詢結果
   */
  async updateOrderByOrderId(db, orderId) {
    let sql = `UPDATE order_table
                 SET state = 'done'
               WHERE order_id = ? AND state = 'undone'`;
    let result = db.runAsync(sql, [orderId]);
    return result;
  }
};